jQuery(document).ready(function () {
    var popularBooks = JSON.parse(localStorage.getItem("popularBooks"));
    jQuery.each(popularBooks.data,function (bid, book) {
        $("#popularBooks").append("\
        <div class='col-sm-2' id='"+book.id+"'>\
        <a style="+clickable()+" href='booking'><img class='img-responsive' src='"+book.image+"' height='200px'></img></a>\
        <h5>"+book.name+"</h5>\
        <h6>Author:"+book.author+"</h6>\
        <h5>Copies: "+book.availableQuantity+"</h5>\
        </div>\
        ");
    });

    var allBooks = JSON.parse(localStorage.getItem("allBooks"));
    jQuery.each(allBooks.data,function (bid, book) {
        $("#allBooksRow").append("\
        <div class='col-sm-2' id='"+book.id+"'>\
        <a style="+clickable()+" href='booking'><img class='img-responsive' src='"+book.image+"' height='200px'></img></a>\
        <h5>"+book.name+"</h5>\
        <h6>Author:"+book.author+"</h6>\
        <h5>Copies: "+book.availableQuantity+"</h5>\
        </div>\
        ");
    });

    $(".img-responsive").click(function (event) {
        event.preventDefault();
        console.log($(".img-responsive").prop("src"));
        var str = $(".img-responsive").parent().prop("id");
        createCookie("booking",str,3);
        //window.location = "booking";
        //srediti ovo za dinamicko ucitavanje booking.html kad je user ulogovan

    });
});

function clickable() {
    var sessionCookie = readCookie('session');
    if(sessionCookie!=null && sessionCookie=='on'){
        return "'cursor: pointer;'";
    }else {
        $('#popularBooks').on('click',function(e){
            e.preventDefault();
        });
        $('#allBooksRow').on('click',function(e){
            e.preventDefault();
        });
        return "'cursor: not-allowed;'";
    }
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') {
            c = c.substring(1,c.length);
        }
        if (c.indexOf(nameEQ) == 0) {
            return c.substring(nameEQ.length,c.length);
        }
    }
    return null;
}